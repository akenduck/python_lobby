
import codes
import commands

class Lobby:

    def __init__ (self):
        self.id = "test"
        self.peers = []

    def join(self, peer):
        self.peers.append(peer)
        for target in self.peers:
            target.send_data(codes.LOBBY_CODE,  commands.LOBBY_COMMAND_INFO, 0, self.get_data())

    def leave(self, peer):
        self.peers.remove(peer)
        for target in self.peers:
            target.send_data(codes.LOBBY_CODE,  commands.LOBBY_COMMAND_INFO, 0, self.get_data())

    def get_data(self):
        return {
            "id" : self.id,
            "peers" : self.get_peers_data()
        }

    def get_peers_data(self):
        data = []
        for peer in self.peers:
            data.append({"id" : peer.addr})
        return data

    def message_server(self, peer_sender):

        print("message_server")

    def message_one(self, peer_sender):
        for target in self.peers:
            if target.addr == peer_sender.request["receiverId"]:
                target.send_data({ "code" : codes.MESSAGE_CODE, "command" : commands.MESSAGE_COMMAND_ONE, "senderId" : peer_sender.addr, "data" : peer_sender.request["data"]})
        print("message_one")

    def message_all(self, peer_sender):
        for target in self.peers:
            target.send_data({ "code" : codes.MESSAGE_CODE, "command" : commands.MESSAGE_COMMAND_ALL, "senderId" : peer_sender.addr, "data" : peer_sender.request["data"]})
        print("message_all")
