import json
class Command:
        def __init__(self, code, command, id, data):
            self.code = code
            self.command = command
            self.id = id
            self.data = json.dumps(data)

class CommandEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Command):
            return obj.__dict__
        return json.JSONEncoder.default(self, obj)
