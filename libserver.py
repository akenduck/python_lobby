import sys
import selectors
import json
import io
import struct

import lobby_manager
import codes
import command


class Peer:

    def _clear(self):
        self._send_buffer = b""
        self._jsonheader_len = None
        self.jsonheader = None
        self.request = None

    def __init__(self, selector, sock, addr):
        self.selector = selector
        self.sock = sock
        self.addr = addr[0]
        self._recv_buffer = b""
        self._clear()

    def _read(self):
        try:
            # Should be ready to read
            data = self.sock.recv(4096)
        except BlockingIOError:
            # Resource temporarily unavailable (errno EWOULDBLOCK)
            pass
        else:
            if data:
                self._recv_buffer += data
            else:
                print("data ",data)
                lobby_manager.lobby_manager.drop_peer(self)
                return False
        return True

    def _write(self):
        if self._send_buffer:
            print("sending", repr(self._send_buffer), "to", self.addr)
            try:
                # Should be ready to write
                sent = self.sock.send(self._send_buffer)
                print("sent ", sent)
                self._send_buffer = self._send_buffer[sent:]
            except BlockingIOError:
                # Resource temporarily unavailable (errno EWOULDBLOCK)
                pass


    def _json_encode(self, obj, encoding):
        return json.dumps(obj, ensure_ascii=False).encode(encoding)

    def _json_decode(self, json_bytes, encoding):
        print("_json_decode", repr(json_bytes), "to", self.addr)
        tiow = io.TextIOWrapper(
            io.BytesIO(json_bytes), encoding=encoding, newline=""
        )
        obj = json.load(tiow)
        tiow.close()
        return obj

    def _create_message(
        self, *, contentBytes, contentType, contentEncoding
    ):
        jsonheader = {
            "byteorder": sys.byteorder,
            "contentType": contentType,
            "contentEncoding": contentEncoding,
            "contentLength": len(contentBytes),
        }
        jsonheader_bytes = self._json_encode(jsonheader, "utf-8")
        message_hdr = struct.pack("i", len(jsonheader_bytes))
        message = message_hdr + jsonheader_bytes + contentBytes
        return message

    def process_events(self, mask):
        self.alive = True
        if mask & selectors.EVENT_READ:
            print("read")
            self.alive = self.read()
        if mask & selectors.EVENT_WRITE:
            self.write()

    def read(self):
        success = self._read()

        if not success:
            return False

        if self._jsonheader_len is None:
            self.process_protoheader()

        if self._jsonheader_len is not None:
            if self.jsonheader is None:
                self.process_jsonheader()

        if self.jsonheader:
            if self.request is None:
                self.process_request()
        return True


    def close(self):
        print("closing connection to", self.addr)
        try:
            self.selector.unregister(self.sock)
        except Exception as e:
            print(
                f"error: selector.unregister() exception for",
                f"{self.addr}: {repr(e)}",
            )
        if self.sock is None:
            return
        try:
            self.sock.close()
        except OSError as e:
            print(
                f"error: socket.close() exception for",
                f"{self.addr}: {repr(e)}",
            )
        finally:
            # Delete reference to socket object for garbage collection
            self.sock = None
            self.alive = False

    def process_protoheader(self):
        hdrlen = 4
        if len(self._recv_buffer) >= hdrlen:
            self._jsonheader_len = struct.unpack(
                "i", self._recv_buffer[:hdrlen]
            )[0]
            self._recv_buffer = self._recv_buffer[hdrlen:]
            print("header len", self._jsonheader_len)


    def process_jsonheader(self):
        hdrlen = self._jsonheader_len
        if len(self._recv_buffer) >= hdrlen:
            self.jsonheader = self._json_decode(
                self._recv_buffer[:hdrlen], "utf-8"
            )
            self._recv_buffer = self._recv_buffer[hdrlen:]
            for reqhdr in (
                "byteorder",
                "contentLength",
                "contentType",
                "contentEncoding",
            ):
                if reqhdr not in self.jsonheader:
                    raise ValueError(f'Missing required header "{reqhdr}".')

    def process_request(self):
        content_len = self.jsonheader["contentLength"]
        if not len(self._recv_buffer) >= content_len:
            return
        data = self._recv_buffer[:content_len]
        self._recv_buffer = self._recv_buffer[content_len:]
        if self.jsonheader["contentType"] == "text/json":
            encoding = self.jsonheader["contentEncoding"]
            self.request = self._json_decode(data, encoding)
            print("received request", repr(self.request), "from", self.addr)
        else:
            self.request = data
            print(
                f'received {self.jsonheader["contentType"]} request from',
                self.addr,
            )

        callback = codes[self.request["code"]]
        callback(self)
        self._clear()

    def send_data(self, codeCun, commandNum, messageId, data):
        c = command.Command(codeCun, commandNum, messageId, data)
        print(self._json_encode(data, "utf-8"))
        response = {
            "contentBytes": bytes(json.dumps(c, cls=command.CommandEncoder), 'utf-8'),
            "contentType": "text/json",
            "contentEncoding": "utf-8",
        }
        message = self._create_message(**response)
        self._send_buffer += message
        self._write()

codes = {
    codes.LOBBY_CODE : lobby_manager.lobby_manager.lobby_command,
    codes.MESSAGE_CODE : lobby_manager.lobby_manager.message_command
}
