import libserver
import lobby
import codes
import commands

class LobbyManager:

    def print(self):
        print("LobbyManager")

    def get_lobby(self, peer):
        print("peer - " + repr(peer))
        for lobby in lobbies:
            if peer in lobbies[lobby].peers:
                return lobby

        return None

    def drop_peer(self, peer):
        self.lobby_leave(peer, self.get_lobby(peer))

    def lobby_command(self, peer):
        callback = lobby_commands[peer.request["command"]]
        callback(peer)


    def get_data(self):
        data = []
        for lobby in lobbies:
            data.append(lobbies[lobby].get_data())
        return data

    def lobby_list(self, peer):
        peer.send_data(codes.LOBBY_CODE,  commands.LOBBY_COMMAND_LIST, 0, self.get_data())

    def lobby_join(self, peer):
        l = lobbies[peer.request["lobby"]]
        l.join(peer)

    def lobby_host(self, peer):
        l = lobby.Lobby()
        l.join(peer)
        #peer.send_data(codes.LOBBY_CODE,  commands.LOBBY_COMMAND_LIST, 0, self.get_data())

        lobbies[l.id] = l

    def lobby_leave(self, peer, lobby_id = None):
        id = lobby_id
        if lobby_id is None:
            if peer.request is not None:
                id = peer.request["lobby"]
        if id is None:
            return
        l = lobbies[id];
        l.leave(peer)
        if len(l.peers) < 1:
            del lobbies[id]

    def lobby_info(self, peer):
        peer.send_data(codes.LOBBY_CODE,  commands.LOBBY_COMMAND_INFO, 0, lobbies[peer.request["lobby"]].get_data())

    def message_command(self, peer):
        callback = message_commands[peer.request["command"]]
        callback(peer)

    def message_server(self, peer):
        lobby = self.get_lobby(peer)
        lobbies[lobby].message_server(peer)

    def message_one(self, peer):
        lobby = self.get_lobby(peer)
        lobbies[lobby].message_one(peer)

    def message_all(self, peer):
        lobby = self.get_lobby(peer)
        lobbies[lobby].message_all(peer)


lobby_manager = LobbyManager()

lobbies = {

}

lobby_commands = {
    commands.LOBBY_COMMAND_LIST:lobby_manager.lobby_list,
    commands.LOBBY_COMMAND_JOIN:lobby_manager.lobby_join,
    commands.LOBBY_COMMAND_HOST:lobby_manager.lobby_host,
    commands.LOBBY_COMMAND_LEAVE:lobby_manager.lobby_leave,
    commands.LOBBY_COMMAND_INFO:lobby_manager.lobby_info
}

message_commands = {
    commands.MESSAGE_COMMAND_SERVER:lobby_manager.message_server,
    commands.MESSAGE_COMMAND_ONE:lobby_manager.message_one,
    commands.MESSAGE_COMMAND_ALL:lobby_manager.message_all
}
